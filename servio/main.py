#!/usr/bin/env python3

import argparse
import subprocess
import sys
import os
import logging
from tempfile import TemporaryFile
from .conf import load_conf, ConfError
from .version import __version__


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "conf",
        help="Configuration file (or - for stdin)",
    )

    parser.add_argument(
        "-v", dest="verbose",
        default=0,
        help="Verbose",
        action="count"
    )

    parser.add_argument(
        "-V", "--version",
        action="version",
        version="%(prog)s {}".format(__version__),
    )

    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(format="servio %(levelname)s: %(message)s")
    logger.setLevel(args.verbose)

    try:
        conf = load_conf(args.conf)
    except ConfError as e:
        logging.error("%s", e)
        return -1

    logger.debug("Conf: %s", repr(conf))

    try:
        p_stdout = run_processors(conf.input_, conf.processors)

        if conf.notificators:
            run_notificators(p_stdout, conf.notificators)
        else:
            sys.stdout.buffer.write(p_stdout)
    except ProcessorError as e:
        logger.error(e)
        return e.retcode
    except NotificatorError as e:
        return -1


class ProcessorError(Exception):

    def __init__(self, retcode, *args) -> None:
        super().__init__(*args)
        self.retcode = retcode

class NotificatorError(Exception):
    pass

def run_notificators(input_, notificators):
    notificators_failures = [False] * len(notificators)

    processes = []
    for i, notificator in enumerate(notificators):
        try:
            logger.info(
                "Starting %s notificator",
                notificator.path
            )
            cmdline = get_program_cmdline(notificator)
            logger.debug("Notificator: %s", cmdline)

            in_file = open_tempfile(input_)
            process = subprocess.Popen(
                cmdline,
                stdin=in_file,
            )
            processes.append((True, process))
        except OSError as e:
            logger.error(
                "Notificator[{}] {} error: {}".format(
                    i,
                    notificator.path,
                    e
                ))
            notificators_failures[i] = True
            processes.append((False, None))

    for i, (is_process, process) in enumerate(processes):
        if not is_process:
            continue

        retcode = process.wait()
        if retcode != 0:
            logger.error(
                "Notificator[{}] {} exit code: {}".format(
                    i,
                    process.args[0],
                    retcode
            ))
            notificators_failures[i] = True

    if all(notificators_failures):
        raise NotificatorError()

def run_processors(input_, processors):
    p_stdin = open_tempfile(input_.encode())

    processes = []
    for i, processor in enumerate(processors):
        try:
            logger.info("Starting %s processor", processor.path)
            cmdline = get_program_cmdline(processor)
            logger.debug("Processor: %s", cmdline)
            process = subprocess.Popen(
                cmdline,
                stdin=p_stdin,
                stdout=subprocess.PIPE
            )
            p_stdin = process.stdout

            processes.append(process)
        except OSError as e:
            raise ProcessorError(-1,
                "Processor[{}] {} error: {}".format(
                    i,
                    processor.path,
                    e
                ))

    p_stdout, _ = process.communicate()

    for i, process in enumerate(processes):
        retcode = process.wait()
        if retcode != 0:
            raise ProcessorError(
                retcode,
                "Processor[{}] {} exit code: {}".format(
                    i,
                    process.args[0],
                    retcode
            ))

    return p_stdout


def open_tempfile(data):
    tf = TemporaryFile()
    tf.write(data)
    tf.seek(0)

    return tf


def get_program_cmdline(program_conf):
    cmdline = [program_conf.path]

    cmdline.extend(command_args_to_list(program_conf.args))

    if program_conf.subcommand is not None:
        cmdline.extend(get_command_cmdline(program_conf.subcommand))

    return cmdline

def get_command_cmdline(subcommand):
    cmdline = [subcommand.name]

    cmdline.extend(command_args_to_list(subcommand.args))

    if subcommand.subcommand is not None:
        cmdline.extend(get_command_cmdline(subcommand.subcommand))

    return cmdline

def command_args_to_list(command_args):
    args_list = []

    args_list.extend(command_args.flags)
    for name, value in command_args.optionals:
        args_list.extend([name, value])

    if command_args.positionals is not None:
        args_list.extend(command_args.positionals)

    return args_list

if __name__ == '__main__':
    exit(main())
