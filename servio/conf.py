import yaml
import sys
import shlex

class ParseError(Exception):

    def __init__(self, msg, path=None):
        super().__init__(msg)
        self.path = []
        if path:
            self.path.append(path)

    def prepend_path(self, v):
        self.path.insert(0,v)

    def __str__(self):
        return "{} : {}".format("/".join(self.path), super().__str__())


def load_conf(conf_path):
    try:
        if conf_path == "-":
            conf_d = yaml.safe_load(sys.stdin)
        else:
            with open(conf_path) as fi:
                conf_d = yaml.safe_load(fi)
        conf = _parse_conf(conf_d)
        return conf
    except yaml.YAMLError as e:
        raise ConfError("Error loading {}: {}".format(conf_path, e))
    except ParseError as e:
        raise ConfError("Error parsing {}::/{}".format(conf_path, e))


def _parse_conf(conf_d):
    if not isinstance(conf_d, dict):
        raise ParseError("Expected dictionary, got: {}".format(conf_d))

    try:
        input_ = _parse_input(conf_d.get("input", ""))
    except ParseError as e:
        e.prepend_path("input")
        raise e

    try:
        processors = _parse_programs(conf_d.get("processors", []))
    except ParseError as e:
        e.prepend_path("processors")
        raise e

    try:
        notificators = _parse_programs(conf_d.get("notificators", []))
    except ParseError as e:
        e.prepend_path("notificators")
        raise e

    return Conf(input_, processors, notificators)

class ConfError(Exception):

    def __init__(self, msg):
        super(Exception, self).__init__(msg)

def _parse_programs(proc_l):
    _check_is_list(proc_l)

    procs = []
    for i, proc in enumerate(proc_l):
        try:
            procs.append(_parse_program(proc))
        except ParseError as e:
            e.prepend_path(str(i))
            raise e

    return procs

def _parse_input(in_s):
    if not isinstance(in_s, str):
        raise ParseError("Expected string, got: {}".format(
            in_s.__class__.__name__
        ))
    return in_s

def _parse_program(prog):
    if isinstance(prog, dict):
        return _parse_program_dict(prog)
    elif isinstance(prog, str):
        return _parse_program_list(shlex.split(prog))
    elif isinstance(prog, list):
        return _parse_program_list(prog)

    raise ParseError("Expected dict, list or string, got: {}".format(
            prog.__class__.__name__
    ))

def _parse_program_list(prog_l):
    if not prog_l:
            raise ParserError("Supply some command")
    return Program(
        path=prog_l[0],
        args=Args(flags=[], optionals={}, positionals=prog_l[1:]),
        subcommand=None
    )

def _parse_program_dict(proc_d):
    path = _get_required_string(proc_d, "path")

    if "subcommand" in proc_d and proc_d["subcommand"] is not None:
        try:
            subcommand = _parse_command(proc_d["subcommand"])
        except ParseError as e:
            e.prepend_path("subcommand")
            raise e
    else:
        subcommand = None

    try:
        args = _parse_args(proc_d.get("args") or {})
    except ParseError as e:
        e.prepend_path("args")
        raise e

    return Program(
        path=path,
        args=args,
        subcommand=subcommand
    )

def _parse_command(cmd):
    if isinstance(cmd, dict):
        return _parse_command_dict(cmd)
    elif isinstance(cmd, str):
        return _parse_command_list(shlex.split(cmd))
    elif isinstance(cmd, list):
        return _parse_command_list(cmd)

    raise ParseError("Expected dict, list or string, got: {}".format(
            cmd.__class__.__name__
    ))

def _parse_command_list(prog_l):
    if not prog_l:
            raise ParserError("Supply some command")
    return Command(
        name=prog_l[0],
        args=Args(flags=[], optionals={}, positionals=prog_l[1:]),
        subcommand=None
    )

def _parse_command_dict(cmd_d):

    name = _get_required_string(cmd_d, "name")

    if "subcommand" in cmd_d and cmd_d["subcommand"] is not None:
        try:
            subcommand = _parse_command(cmd_d["subcommand"])
        except ParseError as e:
            e.prepend_path("subcommand")
            raise e
    else:
        subcommand = None

    try:
        args = _parse_args(cmd_d.get("args") or {})
    except ParseError as e:
        e.prepend_path("args")
        raise e

    return Command(
        name=name,
        args=args,
        subcommand=subcommand
    )

def _parse_args(args_d):
    _check_is_dict(args_d)

    positionals = []
    optionals = {}
    flags = set()

    for name, value in args_d.items():
        name = str(name)

        # positional arguments
        if name == "":
            if isinstance(value, list):
                positionals = value
            else:
                positionals = [value]
            continue

        if not name.startswith("-"):
            if len(name) == 1:
                name = "-{}".format(name)
            else:
                name = "--{}".format(name)

        if value is None:
            flags.add(name)
        else:
            optionals[name] = value

    return Args(flags=flags, optionals=optionals, positionals=positionals)


def _check_is_dict(d):
    if not isinstance(d, dict):
        raise ParseError("Expected dictionary, got: {}".format(
            d.__class__.__name__
        ))

def _check_is_list(l):
    if not isinstance(l, list):
        raise ParseError("Expected list, got: {}".format(
            l.__class__.__name__
        ))

def _get_required_string(dep_obj, attr):
    try:
        value = dep_obj[attr]
        if not isinstance(value, str):
            raise ParseError("must be a string", attr)
    except KeyError:
        raise ParseError("Expected attribute: {}".format(attr))

    return value

class Conf:

    def __init__(self, input_, processors, notificators):
        self.input_ = input_
        self.processors = processors
        self.notificators = notificators

    def __repr__(self):
        return "{}{}".format("Conf", repr(self.__dict__))

class Program:

    def __init__(self, path, args, subcommand):
        self.path = path
        self.args = args
        self.subcommand = subcommand

    def __repr__(self):
        return "{}{}".format("Program", repr(self.__dict__))

class Command:

    def __init__(self, name, args, subcommand):
        self.name = name
        self.args = args
        self.subcommand = subcommand

    def __repr__(self):
        return "{}{}".format("Command", repr(self.__dict__))

class Args:

    def __init__(self, flags, optionals, positionals):
        self.flags = flags
        self.optionals = optionals
        self.positionals = positionals

    def __repr__(self):
        return "{}{}".format("Args", repr(self.__dict__))
