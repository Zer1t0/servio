# Servio

Define pipelines in yaml files

## Usage

```
servio pipeline.yml
```

## Configuration file

Each file describes a command pipeline, similar to those in bash files.

The data flow goes this way:

```
                    .+
        .----------------------.  .-> notificator
input --|--> processor --data--|--|-> notificator
        '----------------------'  '-> notificator
```

### Input
The input is an string that will be passed to the first collector by stdin.
If not input is provided, then an empty string is used.

```yaml
input: Hello world
```

### Processors

The processors will be executed in serie, just line a normal bash pipe.
The first (and only required) processor will receive the input and sent
its output to the next processor (if many) or the notificator.

```yaml
processors:
  - path: ls
    args:
      -l:
      "": /usr/bin
```

### Notificators

The notificators are an optional final step. In case no notificator is defined
the output from last processor will be sent to stdout.

In case notificators are defined, they will be executed in parallel and
execution will fail in case all notificators fail.

```yaml
notificators:
  - path: tee
    args:
      "": /tmp/usr-links.txt
```

## Installation

```
pip3 install git+gitlab.com/Zer1t0/servio
```

