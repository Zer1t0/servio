#!/usr/bin/env python3

import servio

if __name__ == '__main__':
    exit(servio.main())
